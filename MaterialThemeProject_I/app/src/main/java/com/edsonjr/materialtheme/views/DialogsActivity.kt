package com.edsonjr.materialtheme.views

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import com.edsonjr.materialtheme.R
import com.edsonjr.materialtheme.databinding.ActivityDialogsBinding

class DialogsActivity : AppCompatActivity() {

    private lateinit var binding: ActivityDialogsBinding
    private lateinit var alertDialog: AlertDialog.Builder
    private lateinit var dialogItens: AlertDialog.Builder

    private val items = arrayOf("Item 1","Item 2", "Item 3", "Item 4")

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityDialogsBinding.inflate(layoutInflater)
        setContentView(binding.root)

        createSimpleAlert()
        createAlertWithOptions()


        binding.btnAlert.setOnClickListener {
            alertDialog.show()

        }

        binding.btnAlertItens.setOnClickListener {
            dialogItens.show()
        }

    }

    private fun createSimpleAlert() {
        alertDialog = AlertDialog.Builder(this, R.style.AlertDialog)
        alertDialog.setMessage("Deseja Excluir?")

        alertDialog.setPositiveButton("Sim") { _,_ ->
            Toast.makeText(this,"Excluido",Toast.LENGTH_SHORT).show()
        }

        alertDialog.setNegativeButton("Não"){ _,_ ->
            Toast.makeText(this,"Operação abortada",Toast.LENGTH_SHORT).show()
        }

        alertDialog.create()
    }


    private fun createAlertWithOptions() {
        dialogItens = AlertDialog.Builder(this, R.style.AlertDialog)
        dialogItens.setTitle("Selecione a opção desejada")
        dialogItens.setSingleChoiceItems(items,-1) { _, itemIndex ->
            Toast.makeText(this,"Selecionado ${items[itemIndex]}",Toast.LENGTH_SHORT).show()
        }
        dialogItens.create()
        //setMultChoiceItems -> Cria o dialogo com opcoes no formato de checkbox

    }
}
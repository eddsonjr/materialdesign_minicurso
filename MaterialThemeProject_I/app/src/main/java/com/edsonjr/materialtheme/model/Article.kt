package com.edsonjr.materialtheme.model


import java.io.Serializable


data class Article(
    val title: String,
    val description: String,
    val image: String
    ): Serializable
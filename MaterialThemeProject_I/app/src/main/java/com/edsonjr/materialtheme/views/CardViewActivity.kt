package com.edsonjr.materialtheme.views

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.edsonjr.materialtheme.databinding.ActivityCardViewBinding

class CardViewActivity : AppCompatActivity() {

    private lateinit var binding: ActivityCardViewBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityCardViewBinding.inflate(layoutInflater)
        setContentView(binding.root)
    }
}
package com.edsonjr.materialtheme.model

class ArticleDataStore {
    companion object {
        val articleDataStore: List<Article> = listOf(
            Article(
                title = "Kotlin para todos os lados!",
                description = "Veja o uso massivo de kotlin para dispositivos móveis, web e agora para IA e embarcado!",
                image = "https://silvrback.s3.amazonaws.com/uploads/0f11ba64-5b55-4e0f-b4fb-4c5faf611c7b/kotlin_800x320.png" ),

            Article(
                title = "Kotlin para web - Ktor e Spring Boot",
                description = "Aprenda a construir os seus web services e APIs com kotlin!",
                image = "https://discoversdkcdn.azureedge.net/postscontent/springKotlin2.png" ),

            Article(
                title = "Flutter",
                description = "Aprenda a construir aplicativos móveis com flutter",
                image = "https://www.opus-software.com.br/wp-content/uploads/2021/01/imagem-destaque-Flutter.jpg" )

        )
    }
}
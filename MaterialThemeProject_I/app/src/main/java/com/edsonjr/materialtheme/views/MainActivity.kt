package com.edsonjr.materialtheme.views

import android.app.Activity
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.edsonjr.materialtheme.databinding.ActivityMainBinding

class MainActivity : AppCompatActivity() {

    private lateinit var binding: ActivityMainBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)


        binding.materialInputText1.setOnClickListener {
            loadActivity(MaterialEditTextActivity())
        }

        binding.buttons.setOnClickListener{
            loadActivity(ButtonsActivity())
        }


        binding.toolbar.setOnClickListener{
            loadActivity(ToolBarActivity())
        }


        binding.drawerMenu.setOnClickListener{
            loadActivity(DrawerActivity())
        }


        binding.selectionControls.setOnClickListener{
            loadActivity(SelectionControlsActivity())
        }


        binding.dialogs.setOnClickListener{
            loadActivity(DialogsActivity())
        }

        binding.progressbar.setOnClickListener{
            loadActivity(ProgressBarActivity())
        }


        binding.cardViews.setOnClickListener{
            loadActivity(CardViewActivity())
        }


        binding.animation.setOnClickListener{
            loadActivity(AnimationActivity())
        }


        binding.bottomNavigationBar.setOnClickListener {
            loadActivity(BottomNavigationActivity())
        }


        binding.collapsingLayout.setOnClickListener{
            loadActivity(CollapsingLayoutActivity())
        }


        binding.viewPager.setOnClickListener {
            loadActivity(ViewPagerActivity())
        }

        binding.recyclerView.setOnClickListener {
            loadActivity(RecyclerViewActivity())
        }
    }

    private fun loadActivity(activity: Activity){
        val intent = Intent(this,activity::class.java)
        startActivity(intent)
    }

}
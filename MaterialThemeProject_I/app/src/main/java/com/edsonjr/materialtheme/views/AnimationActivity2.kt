package com.edsonjr.materialtheme.views

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.edsonjr.materialtheme.databinding.ActivityAnimation2Binding
import com.edsonjr.materialtheme.databinding.ActivityAnimation3Binding

class AnimationActivity2 : AppCompatActivity() {

    private lateinit var binding: ActivityAnimation3Binding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityAnimation3Binding.inflate(layoutInflater)
        setContentView(binding.root)
    }
}
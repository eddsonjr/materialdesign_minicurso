package com.edsonjr.materialtheme.views

import android.os.AsyncTask
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.ProgressBar
import com.edsonjr.materialtheme.databinding.ActivityProgressBarBinding

class ProgressBarActivity : AppCompatActivity() {

    private lateinit var binding: ActivityProgressBarBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityProgressBarBinding.inflate(layoutInflater)
        setContentView(binding.root)



        binding.startCircularProgressBar.setOnClickListener {
            asyncTask(binding.circularProgressBarComponent).execute()
        }

        binding.startLinearProgressBar.setOnClickListener {
            binding.linearProgressBarComponent.setProgress(0)
            binding.linearProgressBarComponent.max = 100
            asyncTask(binding.linearProgressBarComponent).execute()

        }

    }


    //Criando uma task em background
    class asyncTask(private val progressBar: ProgressBar): AsyncTask<Void,Int,Void>() {

        override fun doInBackground(vararg p0: Void?): Void? {
            for(i in 0..100){
                try {
                    publishProgress(i)
                    Thread.sleep(100)
                }catch (e: InterruptedException) {
                    Log.e("ProgressBar",e.printStackTrace().toString())
                }

            }
            return null
        }

        override fun onPreExecute() {
            progressBar.visibility = View.VISIBLE
        }

        override fun onPostExecute(result: Void?) {
            progressBar.visibility = View.GONE
        }

        override fun onProgressUpdate(vararg values: Int?) {
            progressBar.progress = values[0]!!
        }

    }
}
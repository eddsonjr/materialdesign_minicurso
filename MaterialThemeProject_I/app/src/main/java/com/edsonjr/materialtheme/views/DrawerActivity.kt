package com.edsonjr.materialtheme.views

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import androidx.appcompat.app.ActionBarDrawerToggle
import androidx.core.view.GravityCompat
import com.edsonjr.materialtheme.R
import com.edsonjr.materialtheme.databinding.ActivityDrawerBinding

class DrawerActivity : AppCompatActivity() {

    private lateinit var binding: ActivityDrawerBinding
    private lateinit var toolbar: androidx.appcompat.widget.Toolbar

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityDrawerBinding.inflate(layoutInflater)
        setContentView(binding.root)


        //configurando a Toolbar
        this.toolbar = findViewById(R.id.toolbar)
        setSupportActionBar(toolbar)

        //menu na toolbar para chamar o drawer menu
        val toogle = ActionBarDrawerToggle(
            this,
            binding.drawerLayout,
            toolbar,
            R.string.open_drawer,
            R.string.close_drawer
        )
        binding.drawerLayout.addDrawerListener(toogle)
        toogle.syncState()


        //click do menu do drawer menu
        binding.navigationView.setNavigationItemSelectedListener {
            val id = it.itemId

            when(id){
                R.id.menu_inbox -> { Toast.makeText(this,"Clicado em Inbox",Toast.LENGTH_SHORT).show()}
                R.id.menu_starret -> { Toast.makeText(this,"Clicado em Favoritos",Toast.LENGTH_SHORT).show()}
                R.id.menu_sent_emails -> { Toast.makeText(this,"Clicado em emails enviados",Toast.LENGTH_SHORT).show()}
                R.id.menu_trash -> { Toast.makeText(this,"Clicado em Lixeira",Toast.LENGTH_SHORT).show()}
                R.id.menu_spam -> { Toast.makeText(this,"Clicado em Spam",Toast.LENGTH_SHORT).show()}
            }
            true
        }


    }

    //aplicando uma acao ao backpressed para fechar o drawer menu ao clicar em voltar
    override fun onBackPressed() {
        if(binding.drawerLayout.isDrawerOpen(GravityCompat.START)){
            binding.drawerLayout.closeDrawer(GravityCompat.START)

        }else {
            super.onBackPressed()
        }
    }
}
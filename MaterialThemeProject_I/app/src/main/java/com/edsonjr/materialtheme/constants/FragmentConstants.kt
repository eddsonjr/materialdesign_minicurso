package com.edsonjr.materialtheme.constants

class FragmentConstants {
    companion object {
        val VIEW_PAGER_FRAGMENT_KEY = "view_pager_fragment_key"
    }
}
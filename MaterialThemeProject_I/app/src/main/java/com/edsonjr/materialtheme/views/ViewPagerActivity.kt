package com.edsonjr.materialtheme.views

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.edsonjr.materialtheme.R
import com.edsonjr.materialtheme.adapters.SimpleViewPagerAdapter
import com.edsonjr.materialtheme.databinding.ActivityViewPagerBinding
import com.edsonjr.materialtheme.model.ArticleDataStore.Companion.articleDataStore
import com.edsonjr.materialtheme.views.fragments.ViewPagerFragmentOne
import com.edsonjr.materialtheme.views.fragments.ViewPagerFragmentTwo
import com.google.android.material.tabs.TabLayoutMediator

class ViewPagerActivity : AppCompatActivity() {

    private lateinit var binding: ActivityViewPagerBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityViewPagerBinding.inflate(layoutInflater)
        setContentView(binding.root)

        //Criando o adapter para trabalhar com o ViewPager
        val viewPagerAdapter = SimpleViewPagerAdapter(
            activity = this,
            argumentsToFragment = articleDataStore)
        binding.viewPager.adapter = viewPagerAdapter

        //configurando o tablayout
        val tabTitles: MutableList<Pair<String,Int>> = ArrayList()
        tabTitles.add(Pair("Frag 1",R.drawable.ic_dot))
        tabTitles.add(Pair("Frag 2",R.drawable.ic_dot))
        TabLayoutMediator(binding.tabLayout,binding.viewPager){ tab,position ->
            tab.text = tabTitles[position].first
            tab.setIcon(tabTitles[position].second)
        }.attach()



    }
}
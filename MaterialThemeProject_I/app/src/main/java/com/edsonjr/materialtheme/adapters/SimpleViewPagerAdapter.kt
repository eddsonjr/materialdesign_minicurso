package com.edsonjr.materialtheme.adapters

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.viewpager2.adapter.FragmentStateAdapter
import com.edsonjr.materialtheme.constants.FragmentConstants.Companion.VIEW_PAGER_FRAGMENT_KEY
import com.edsonjr.materialtheme.model.Article
import com.edsonjr.materialtheme.views.fragments.ViewPagerFragmentOne
import com.edsonjr.materialtheme.views.fragments.ViewPagerFragmentTwo
import java.io.Serializable

const val NUMBER_OF_FRAGMENTS = 2
class SimpleViewPagerAdapter(activity: AppCompatActivity,
                             private val argumentsToFragment: List<Article>): FragmentStateAdapter(activity){

    override fun getItemCount(): Int = NUMBER_OF_FRAGMENTS

    override fun createFragment(position: Int): Fragment {
        var fragment = Fragment()
        var bundle = Bundle()
        when(position) {
            0 -> {
                fragment = ViewPagerFragmentOne()
                bundle.putSerializable(VIEW_PAGER_FRAGMENT_KEY,argumentsToFragment as Serializable)
                fragment.arguments = bundle
               }
            1 -> {
                fragment =  ViewPagerFragmentTwo()
                bundle.putSerializable(VIEW_PAGER_FRAGMENT_KEY,argumentsToFragment as Serializable)
                fragment.arguments = bundle
                }
        }
        return fragment
    }
}
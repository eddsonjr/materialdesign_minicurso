package com.edsonjr.materialtheme.adapters

import android.view.View
import androidx.recyclerview.widget.RecyclerView
import coil.load
import coil.transform.CircleCropTransformation
import com.edsonjr.materialtheme.R
import com.edsonjr.materialtheme.databinding.ItemRecyclerviewBinding
import com.edsonjr.materialtheme.model.Article


class ViewHolder(private val itemBinding: ItemRecyclerviewBinding)
    : RecyclerView.ViewHolder(itemBinding.root) {

        fun bind(item: Article, clickCallback: (String) -> Unit = {}) {
            itemBinding.title.text = item.title
            itemBinding.desc.text = item.description
            itemBinding.featureImage.load(item.image){
                crossfade(true)
                placeholder(R.drawable.ic_no_image)
                //transformations(CircleCropTransformation()) - Deixa a imagem redonda
            }

            itemBinding.btnLink.setOnClickListener {
                //callback do click do botao de acessar
                clickCallback.invoke(item.title)
            }
        }
}
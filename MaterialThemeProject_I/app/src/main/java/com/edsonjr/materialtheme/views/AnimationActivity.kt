package com.edsonjr.materialtheme.views

import android.app.ActivityOptions
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.transition.*
import android.view.View
import com.edsonjr.materialtheme.R
import com.edsonjr.materialtheme.databinding.ActivityAnimationBinding

class AnimationActivity : AppCompatActivity() {

    private lateinit var binding: ActivityAnimationBinding

    //Criando cenas - Animacao de transicao entre cenas
    private lateinit var scene1: Scene
    private lateinit var scene2: Scene
    private var start: Boolean = true //controla a transicao de cenas

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityAnimationBinding.inflate(layoutInflater)
        setContentView(binding.root)

        //adquirindo as cenas
        scene1 = Scene.getSceneForLayout(binding.root, R.layout.activity_animation,this)
        scene2 = Scene.getSceneForLayout(binding.root, R.layout.activity_animation2,this)


        //click do botao de animar, responsavel por animar a imageview
        binding.btnAnimar.setOnClickListener {

            //Faz a imageview aparecer e desaparecer com efeito de fade
            TransitionManager.beginDelayedTransition(binding.root,Fade())

            //Faz a imagem deslizar para baixo e para cima
            //TransitionManager.beginDelayedTransition(binding.root,Slide())

            binding.imageView.visibility = if(binding.imageView.visibility == View.INVISIBLE) View.VISIBLE else View.INVISIBLE

        }


        //click da imagem, responsavel por carregar a proxima activity animando a transicao
        binding.imageView.setOnClickListener {
            val bundleAnimation: Bundle = ActivityOptions.makeSceneTransitionAnimation(this).toBundle()
            val intent: Intent = Intent(this, AnimationActivity2::class.java)
            startActivity(intent,bundleAnimation)
        }

    }

    //responsavel por animar e transitar entre as cenas
    fun changeScene(view: View) {
        //transicao customizada
        val customTransition = TransitionInflater.from(this).inflateTransition(R.transition.custom_transition)

        if(start){
            TransitionManager.go(scene2,customTransition)
            start = false
        }else {
            TransitionManager.go(scene1,customTransition)
            start = true
        }
    }
}
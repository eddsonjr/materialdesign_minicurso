package com.edsonjr.materialtheme.views

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import android.widget.Toast
import com.edsonjr.materialtheme.R

class ToolBarActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_tool_bar)
        setSupportActionBar(findViewById(R.id.toolbar))
    }


    //Infla o menu
    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.menu_toolbar,menu)
        return true
    }


    //acoes dos botoes do menu
    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when(item.itemId){
            R.id.search_menu -> {Toast.makeText(this,"Clicado em Search",Toast.LENGTH_SHORT).show()}
            R.id.cart_menu -> {Toast.makeText(this,"Clicado em Cart",Toast.LENGTH_SHORT).show()}
            R.id.about_menu -> {Toast.makeText(this,"Clicado em About",Toast.LENGTH_SHORT).show()}
            R.id.config_menu -> {Toast.makeText(this,"Clicado em Config",Toast.LENGTH_SHORT).show()}
        }

        return super.onOptionsItemSelected(item)
    }


}
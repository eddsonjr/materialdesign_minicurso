package com.edsonjr.materialtheme.views

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import androidx.appcompat.widget.AppCompatEditText
import com.edsonjr.materialtheme.R
import com.google.android.material.textfield.TextInputLayout

class MaterialEditTextActivity : AppCompatActivity() {

    private lateinit var editTextEmail: AppCompatEditText
    private lateinit var editTextPassword: AppCompatEditText
    private lateinit var textLayoutEmail: TextInputLayout
    private lateinit var textLayoutPassword: TextInputLayout
    private lateinit var loginBotao: Button


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_material_edit_text)



        //campos de edit Text
        editTextEmail = findViewById(R.id.edit_email)
        editTextPassword = findViewById(R.id.edit_password)


        //campos para input layout
        textLayoutEmail = findViewById(R.id.textLayout_email)
        textLayoutPassword = findViewById(R.id.textLayout_password)

        //botao
        loginBotao = findViewById(R.id.login_button)


        //clicklistener
        loginBotao.setOnClickListener {
            validadeForm()
        }
    }

    private fun validadeForm() {
        if(editTextEmail.text.toString().isNullOrEmpty()){
            textLayoutEmail.isErrorEnabled = true
            textLayoutEmail.error = "Preencha o seu e-mail"
        }else{
            textLayoutEmail.isErrorEnabled = false
        }

        if(editTextPassword.text.toString().isNullOrEmpty()){
            textLayoutPassword.isErrorEnabled = true
            textLayoutPassword.error = "Preencha sua senha"
        }else {
            textLayoutPassword.isErrorEnabled = false
        }
    }


}
package com.edsonjr.materialtheme.views

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import android.widget.Toast
import com.edsonjr.materialtheme.R
import com.edsonjr.materialtheme.databinding.ActivityCollapsingLayoutBinding

class CollapsingLayoutActivity : AppCompatActivity() {

    private lateinit var binding: ActivityCollapsingLayoutBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityCollapsingLayoutBinding.inflate(layoutInflater)
        setContentView(binding.root)

        setSupportActionBar(binding.toolbar)
        supportActionBar?.setDisplayHomeAsUpEnabled(true) //adiciona o botao de voltar na toolbar
        binding.toolbar.setNavigationOnClickListener { onBackPressed() } // executa o backpressed ao clicar na seta de voltar na toolbar
    }


    //Infla e carrega o menu
    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.collapsing_menu_toolbar,menu)
        return super.onCreateOptionsMenu(menu)
    }


    //acoes de cliques do menu da toolbar
    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when(item.itemId){
            R.id.menu_editar_collapsing -> {Toast.makeText(this,"Selecionado editar",Toast.LENGTH_SHORT).show()}
            R.id.menu_favoritar_collpasing -> {Toast.makeText(this,"Selecionado favoritar",Toast.LENGTH_SHORT).show()}
        }
        return true
    }


}
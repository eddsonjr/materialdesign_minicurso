package com.edsonjr.materialtheme.views

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import com.edsonjr.materialtheme.R
import com.edsonjr.materialtheme.databinding.ActivitySelectionControlsBinding

class SelectionControlsActivity : AppCompatActivity() {

    private lateinit var binding: ActivitySelectionControlsBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivitySelectionControlsBinding.inflate(layoutInflater)
        setContentView(binding.root)


        val switch = binding.mySwitch

        switch.setOnCheckedChangeListener { compoundButton, b ->
            if (b){
                // when switch button is checked
                Toast.makeText(this,"Switch is on!",Toast.LENGTH_SHORT).show()
            }else{
                // if switch button is unchecked
                Toast.makeText(this,"Switch off!",Toast.LENGTH_SHORT).show()
            }
        }


        val checkbox = binding.myCheckbox

        checkbox.setOnCheckedChangeListener { compoundButton, b ->
            if (b){
                // when switch button is checked
                Toast.makeText(this,"Checked!",Toast.LENGTH_SHORT).show()
            }else{
                // if switch button is unchecked
                Toast.makeText(this,"Not Checked!",Toast.LENGTH_SHORT).show()
            }
        }

        val radioGroup = binding.radioGroup
        radioGroup.setOnCheckedChangeListener { radioGroup, i ->
           when(i) {
               R.id.option1 -> {Toast.makeText(this,"Opção 1",Toast.LENGTH_SHORT).show()}
               R.id.option2 -> {Toast.makeText(this,"Opção 2",Toast.LENGTH_SHORT).show()}
               R.id.option3 -> {Toast.makeText(this,"Opção 3",Toast.LENGTH_SHORT).show()}
           }

        }
    }
}
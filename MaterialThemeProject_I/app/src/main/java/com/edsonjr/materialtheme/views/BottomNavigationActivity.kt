package com.edsonjr.materialtheme.views

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import com.edsonjr.materialtheme.R
import com.edsonjr.materialtheme.databinding.ActivityBottomNavigationBinding

class BottomNavigationActivity : AppCompatActivity() {

    private lateinit var binding: ActivityBottomNavigationBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityBottomNavigationBinding.inflate(layoutInflater)
        setContentView(binding.root)

        //Responsavel pelos clicks nos itens da bottom navigation
        binding.bottomNavigation.setOnItemSelectedListener { menuId ->
            when(menuId.itemId){
                R.id.menu_home -> {Toast.makeText(this,"Menu_Home",Toast.LENGTH_SHORT).show()}
                R.id.menu_fav -> {Toast.makeText(this,"Menu_favoritos",Toast.LENGTH_SHORT).show()}
                R.id.menu_location -> {Toast.makeText(this,"Menu_Localização",Toast.LENGTH_SHORT).show()}
            }
            return@setOnItemSelectedListener true
        }

    }
}
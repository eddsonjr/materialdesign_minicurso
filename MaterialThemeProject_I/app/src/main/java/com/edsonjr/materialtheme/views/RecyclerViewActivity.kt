package com.edsonjr.materialtheme.views

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import androidx.recyclerview.widget.LinearLayoutManager
import com.edsonjr.materialtheme.adapters.RecyclerAdapter
import com.edsonjr.materialtheme.databinding.ActivityRecyclerViewBinding
import com.edsonjr.materialtheme.model.Article
import com.edsonjr.materialtheme.model.ArticleDataStore

class RecyclerViewActivity : AppCompatActivity() {

    private lateinit var binding: ActivityRecyclerViewBinding
    private val adapter = RecyclerAdapter()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityRecyclerViewBinding.inflate(layoutInflater)
        setContentView(binding.root)

        setupRecyclerView()

    }

    private fun setupRecyclerView() {
        binding.recyclerView.adapter = adapter
        adapter.updateRecyclerView(ArticleDataStore.articleDataStore as MutableList<Article>)
        binding.recyclerView.layoutManager = LinearLayoutManager(this)
        adapter.accessButtonListener = { name ->
            Toast.makeText(this,"Acessando: $name",Toast.LENGTH_SHORT).show()
        }
    }
}
package com.edsonjr.materialtheme.adapters

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.edsonjr.materialtheme.R
import com.edsonjr.materialtheme.databinding.ItemRecyclerviewBinding
import com.edsonjr.materialtheme.model.Article
import com.edsonjr.materialtheme.model.ArticleDataStore

class RecyclerAdapter: RecyclerView.Adapter<ViewHolder>() {

    private var articleList: MutableList<Article> = mutableListOf()
    var accessButtonListener: (String) -> Unit = {}

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val binding =  ItemRecyclerviewBinding.inflate(LayoutInflater.from(parent.context),parent,false)
        return ViewHolder(binding)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(articleList.get(position),accessButtonListener)
    }

    override fun getItemCount(): Int = articleList.size


    fun updateRecyclerView(articleList: MutableList<Article>){
        if(!articleList.isNullOrEmpty()){
            this.articleList.clear()
        }
        this.articleList = articleList
    }
}
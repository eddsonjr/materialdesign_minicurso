package com.edsonjr.materialtheme.views.fragments

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.recyclerview.widget.LinearLayoutManager
import com.edsonjr.materialtheme.R
import com.edsonjr.materialtheme.adapters.RecyclerAdapter
import com.edsonjr.materialtheme.constants.FragmentConstants.Companion.VIEW_PAGER_FRAGMENT_KEY
import com.edsonjr.materialtheme.databinding.FragmentViewPagerTwoBinding
import com.edsonjr.materialtheme.model.Article


class ViewPagerFragmentTwo : Fragment() {

    private var _binding: FragmentViewPagerTwoBinding? = null
    private val binding get() = _binding!!
    private var args: List<Article>? = null
    private lateinit var recyclerViewAdapter: RecyclerAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = FragmentViewPagerTwoBinding.inflate(inflater, container, false)
        getAguments()
        return binding.root
    }

    private fun getAguments() {
        args = arguments?.get(VIEW_PAGER_FRAGMENT_KEY) as List<Article>?
        if(!args.isNullOrEmpty()){
            setupRecyclerViewAdapter()
        }
    }

    private fun setupRecyclerViewAdapter(){
        recyclerViewAdapter = RecyclerAdapter()
        recyclerViewAdapter.updateRecyclerView(args as MutableList<Article>)
        binding.recyclerView.adapter = recyclerViewAdapter
        binding.recyclerView.layoutManager = LinearLayoutManager(requireContext())
        recyclerViewAdapter.accessButtonListener = { name ->
            Toast.makeText(requireContext(),"Acessando: $name", Toast.LENGTH_SHORT).show()
        }

    }
}
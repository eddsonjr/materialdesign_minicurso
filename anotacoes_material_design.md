# Material Design 

## Principais aspectos do Material Design 

O Material Design foi desenvolvido pensando em simular comportamentos do mundo real, tais como sombra, luz, objetos 3D (simulado), etc. Ele também é baseado em "tipografia", ou seja, você define a fonte, tipo de letra, margens, grades, etc. E por fim  ele também possui a possibilidade de movimetanção, ou seja, consegue-se realizar animações com o Material Design.

### O Material Design Simula 3D

Os objetos dispostos pelo Material Design simulam visão 3D. Ele mesmo trabalha  com 3 eixos no plano cartesiano, sendo o eixo Z (o de profundidade) responsável 
por simular profundidade, sendo que neste eixo, os materiais por padrão tem uma espessura uniforme de 1dp. É possível também sobrepor um elemento em relação ao outro, ou seja, trabalhar com a elevação do elemento. Isso fará com que haja uma simulação de um objeto em um espaço 3D. 



### Propriedades básicas do Material Design

1. Propriedades de física
2. Propriedades de transformação do material
3. Movimentação do material (e subsequentemente animações)


__Elevação__: Indica a dinstância entre superfícies e a profundidade da sombra.

__Elevação em Repouso__: Todos os elementos tem a elevação em repouso. 

__Elevação Dinâmica__: É a elevação de um objeto quando um elemento se move em relação ao seu estado de repouso (ex: um botão que ao ser clicado, ele 'salta', ficando em cima dos outros componentes)

### Cores

Pode-se dize que o Material Design trabalha com dois esquemas de cores:

1. __Cor Primária__: refere-se a uma cor que aparece com mais frequência no seu aplicativo. A cor primária também poderá ter váriações de tons (ou seja, outras cores que são variações da cor tida como principal).

2. __Cor Secundária__: refere-se a uma cor usada para centuar partes-chave da interface. A cor secundária, assim como a primária, também pode contar com variações de tons. 

A cor primária é tida como aquela que aparece com mais frequência no aplicativo, estando mais presente em elementos de uma maior visualização, como barras  de menu e navegação, views grandes, etc. Já a cor secundária é utilizada para acentuar partes da interface, tais como botões, botões flutuantes, campos de  texto, barras de progresso, controles de seleção e deslizantes, etc.


### Icones

1. __Ícones de Produto__: representação visual dos produtos, serviços ou ferramentas de uma determinada marca. Em geral, especificamente, estes ícones possuem 48dp. 

2. __Ícones de Sistema__: representam um comando, arquivo, dispositivo ou ações comuns relacionados ao sistema. Em geral, especificamente, estes ícones possuem 24dp de tamanho.


### Tipografia

__Fontes comumente usadas__: Roboto e Noto

#### Algumas especifiações tipográficas

1. __App Bar__: Title Style, Medium 20sp
2. __Buttons__: English: Medium 14sp, preferencial os textos de botões ficam em caixa alta / Dense: Medium 15sp / Tall: Bold 15sp
3. __Subheading__: Regular 16sp / Dense: Regular 17sp / Tall: Regular 17sp
4. __Body__: Regular 14sp / Dense: Regular 15sp / Tall: Regular 15sp



### Unidades e Medidas 

__(Densidade de Pixel)__: Essa unidade refere-se a  quantidade pixels que cabem dentro de 1 polegada. Telas de alta densidade tem mais pixels.

_Densidade da Tela_: largura da tela (ou altura) em pixels / largura da tela (ou altura) em polegadas.

__DP - Pixels independente de densidade__: "Indpependência de densidade" é a exibição uniforme de elementos em telas com diferentes densidades. Os pixels indpendentes de desnidade são unidades flexíveis que se adaptem em qualquer tela. O DP é calculado da seguinte forma:
dp = (largura em pixels * 160) / densidade da tela

__SP - Pixels Escaláveis__: Parecidos com o DP, porém utilizados em fontes e tipografia. Utiliza-se o SP ao invés de DP com as fontes para evitar problemas de distorção da mesma.




### Estrutura básica de telas

Geralmente as telas apresentam os seguintes componentes e estrutura:
1. Tela simples: tela somente com uma ToolBar (barra superior) para navegação
2. Tela com Toolbar e Float Button
3. Tela com TabBar (barra inferior para navegação).

A aplicação pode ser composta por telas contendo ou não todas essas variações.

Toolbars podem ter menus, buscas, navegação.
Há também aplicações que utilizam a Side Nav, ou seja, DrawerMenu (menu lateral)



### Temas
Com os temas é possível alterar cor, sombra, espaçamento dos elementos, formas de elementos, entre outros na sua aplicação Android.Os temas são definidos como: 

1. @android:style/Theme.Material (versão dark)
2. @android:style/Theme.Material.Light (versão light)
3. @android:style/THeme.Material.Light;DarActionBar (seria o tema claro, porém com uma action bar mais escurecida).



### Theme.Material / Theme.AppCompat

Disponível somente no Android 5.0 (API 21) ou posterior. Já o AppCompat pode ser utilizado em android com versão anterior à 5.0. 

É possível aplicar temas individuais para cada activity no seu projeto Android.

### Algumas modificações que ocorrem quando se altera o parent do tema

__style name="Theme.MaterialTheme" parent="Theme.MaterialComponents.DayNight.DarkActionBar"__

![alt text](screenshots/1.png)

---

__style name="Theme.MaterialTheme" parent="Theme.MaterialComponents.DayNight"__

![alt text](screenshots/2.png)


---

__style name="Theme.MaterialTheme" parent="Theme.MaterialComponents"__

![alt text](screenshots/3.png)

---

__style name="Theme.MaterialTheme" parent="Theme.AppCompat"__

![alt text](screenshots/4.png)

---

__style name="Theme.MaterialTheme" parent="Theme.AppCompat.DayNight"__ 
 
![alt text](screenshots/5.png)

---

__style name="Theme.MaterialTheme" parent="Theme.AppCompat.DayNight.DarkActionBar"__

![alt text](screenshots/6.png)
---


---
## Criando temas compatíveis com a versão do Android

Em algums casos, você poderá ter problemas em relação a alguma propriedado dos temas ser incompatível com versões de API baixas do Android. Por exemplo, o atributo _name="android:navigationBarColor"_ responsável por alterar a cor da navigation bar do sistema (aquela barra inferior que tem o botão home, últimos apps e voltar) é compatível somente a partir da versão de API 21. É possível criar temas voltados para versões específicas do Android. Para tanto execute:

1. Na pasta values, crie um novo __Values Resource File__, com o modificador de versão: 

![alt text](screenshots/7.png)

![alt text](screenshots/8.png)



2. Após isso, crie um novo tema neste arquivo recem criado sendo que ele herdará o tema principal da aplicação


__[1] Tema recem criado, compatível com a API >= 21__

```

<?xml version="1.0" encoding="utf-8"?>
<resources>

    <!-- Tema compativel com versoes >= 21 do android -->
    <style name="SuperMaterialTheme" parent="Theme.MaterialTheme">
        <item name="android:navigationBarColor">@color/orange_one</item>
    </style>


</resources>

```


__[2] Tema principal da aplicação, compatível com a versão de API < 21__

```
resources xmlns:tools="http://schemas.android.com/tools">
    <!-- Base application theme. -->
    <style name="Theme.MaterialTheme" parent="Theme.AppCompat.DayNight.DarkActionBar">
        <!-- Primary brand color. -->
        <item name="colorPrimary">@color/purple_500</item>
        <item name="colorPrimaryVariant">@color/purple_700</item>
        <item name="colorOnPrimary">@color/white</item>
        <!-- Secondary brand color. -->
        <item name="colorSecondary">@color/teal_200</item>
        <item name="colorSecondaryVariant">@color/teal_700</item>
        <item name="colorOnSecondary">@color/black</item>
        <!-- Status bar color. -->
        <item name="android:statusBarColor" tools:targetApi="l">?attr/colorPrimaryVariant</item>
    </style>


    <!-- Tema compativel com versoes anteriores a API 21 -->
    <style name="SuperMaterialTheme" parent="Theme.MaterialTheme">

    </style>

```


Note que dentro do arquivo de temas principal, você irá criar um novo tema porém irá deixá-lo vazio. Os items desse novo tema (que é o tema de compatibilidade [2]) serão colocados no outro arquivos de temas. 

3. Altere o Manifest para usar o tema recem criado (o tema de compatiblidade [2]):

```
 android:theme="@style/SuperMaterialTheme">

```

Pronto, agora o Android irá reconhecer qual a API que o device está utilizando e irá carregar o tema correspondente à aquela versão de SO. Neste caso específico, se o device estiver usando a API >= 21, a barra de navegação irá ser alterada, pois o tema contem um atributo para isso. Caso contrário, será carregado o tema de compatibilidade, na qual a cor da barra de navegação é preservada.


## Tela Exemplo 1 - TextField

Para este exemplo, use o esquema de tema já informado acima.

![alt text](screenshots/9.png)


__layout__:
```
<?xml version="1.0" encoding="utf-8"?>
<LinearLayout xmlns:android="http://schemas.android.com/apk/res/android"
    xmlns:app="http://schemas.android.com/apk/res-auto"
    xmlns:tools="http://schemas.android.com/tools"
    android:layout_width="match_parent"
    android:layout_height="match_parent"
    android:orientation="vertical"
    tools:context=".MainActivity">
    
    <com.google.android.material.textfield.TextInputLayout
        android:id="@+id/textLayout_email"
        android:layout_width="match_parent"
        android:layout_height="wrap_content"
        android:layout_marginTop="32dp"
        android:layout_marginRight="24dp"
        android:layout_marginLeft="24dp"
        >

        <androidx.appcompat.widget.AppCompatEditText
            android:id="@+id/edit_email"
            android:layout_width="match_parent"
            android:layout_height="wrap_content"
            android:hint="@string/preencha_seu_e_mail"
            android:inputType="textEmailAddress"
            />


    </com.google.android.material.textfield.TextInputLayout>


    <com.google.android.material.textfield.TextInputLayout
        android:id="@+id/textLayout_password"
        android:layout_width="match_parent"
        android:layout_height="wrap_content"
        android:layout_marginTop="12dp"
        android:layout_marginLeft="24dp"
        android:layout_marginRight="24dp"
        app:counterEnabled="true"
        app:counterMaxLength="8"
        app:passwordToggleEnabled="true">


        <androidx.appcompat.widget.AppCompatEditText
            android:id="@+id/edit_password"
            android:layout_width="match_parent"
            android:layout_height="wrap_content"
            android:hint="@string/informe_sua_senha"
            android:inputType="textPassword"
            android:maxLength="8"
            />

    </com.google.android.material.textfield.TextInputLayout>

    <Button
        android:id="@+id/login_button"
        android:layout_width="match_parent"
        android:layout_height="wrap_content"
        android:layout_marginTop="32dp"
        android:layout_marginLeft="24dp"
        android:layout_marginRight="24dp"
        android:text="@string/login" />

</LinearLayout>

```

__activity__:

```
class MainActivity : AppCompatActivity() {

    private lateinit var editTextEmail: AppCompatEditText
    private lateinit var editTextPassword: AppCompatEditText
    private lateinit var textLayoutEmail: TextInputLayout
    private lateinit var textLayoutPassword: TextInputLayout
    private lateinit var loginBotao: Button

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)


        //campos de edit Text
        editTextEmail = findViewById(R.id.edit_email)
        editTextPassword = findViewById(R.id.edit_password)


        //campos para input layout
        textLayoutEmail = findViewById(R.id.textLayout_email)
        textLayoutPassword = findViewById(R.id.textLayout_password)

        //botao
        loginBotao = findViewById(R.id.login_button)


        //clicklistener
        loginBotao.setOnClickListener {
            validadeForm()
        }

    }

    private fun validadeForm() {
        if(editTextEmail.text.toString().isNullOrEmpty()){
            textLayoutEmail.isErrorEnabled = true
            textLayoutEmail.error = "Preencha o seu e-mail"
        }else{
            textLayoutEmail.isErrorEnabled = false
        }

        if(editTextPassword.text.toString().isNullOrEmpty()){
            textLayoutPassword.isErrorEnabled = true
            textLayoutPassword.error = "Preencha sua senha"
        }else {
            textLayoutPassword.isErrorEnabled = false
        }
    }
}


```

---

## Tela 2 - Buttons

__Layout__:

```
    
<?xml version="1.0" encoding="utf-8"?>
<RelativeLayout xmlns:android="http://schemas.android.com/apk/res/android"
    xmlns:app="http://schemas.android.com/apk/res-auto"
    xmlns:tools="http://schemas.android.com/tools"
    android:layout_width="match_parent"
    android:layout_height="match_parent"
    tools:context=".ButtonsActivity">

    <androidx.appcompat.widget.AppCompatButton
        android:id="@+id/raisedButton"
        android:layout_width="wrap_content"
        android:layout_height="wrap_content"
        android:layout_margin="16dp"
        android:layout_gravity="center"
        android:text="@string/botao_elevado"
        style="@style/RaisedButton"
        android:layout_centerHorizontal="true"
        />


    <androidx.appcompat.widget.AppCompatButton
        android:id="@+id/flatButton"
        android:layout_width="wrap_content"
        android:layout_height="wrap_content"
        android:layout_margin="16dp"
        android:layout_gravity="center"
        android:text="Flat Button"
        style="@style/FlatButton"
        android:layout_centerHorizontal="true"
        android:layout_below="@id/raisedButton"
        />

    <com.google.android.material.floatingactionbutton.FloatingActionButton
        android:id="@+id/fab"
        android:layout_width="wrap_content"
        android:layout_height="wrap_content"
        android:src="@drawable/ic_fab"
        android:layout_margin="16dp"
        android:layout_alignParentBottom="true"
        android:layout_alignParentEnd="true"
        android:layout_alignParentRight="true"
        app:fabSize="normal"
        android:backgroundTint="@color/raised_button_color"
        android:elevation="12dp"
        />

</RelativeLayout>

```

__Temas__: 


```
    <!-- Tema para o primeiro botao -->
    <style name="RaisedButton" parent="Widget.AppCompat.Button.Colored">
        <item name="backgroundTint">@color/raised_button_color</item>
        <item name="android:textColor">@color/raised_button_text_color</item>
        <item name="android:textStyle">bold</item>
        <item name="android:fontFamily">sans-serif-condensed-light</item>
        <item name="android:textAllCaps">false</item>
    </style>

    <!-- Tema para o botao flat. Por padrao este botao nao tem cor de fundo nem bordas -->
    <style name="FlatButton" parent="Widget.AppCompat.Button.Borderless">
        <item name="android:textColor">@color/flatbutton_text_color</item>
    </style>
    
```

__Colors__:


```
    <!-- test colors -->
    <color name="orange_one">#FFC107</color>
    <color name="raised_button_color">#E36533</color>
    <color name="raised_button_text_color">#8CA7BD</color>
    <color name="flatbutton_text_color">#1D79C5</color>

```

![alt text](screenshots/10.png)

---


## Tela 3 - Toolbar (básico)


![alt text](screenshots/11.png)

![alt text](screenshots/12.png)

__Observação__:  O seu tema principal deve ser algum que extenda de _NoActionBar_

__Layout__: 

```
<!-- Adicione no sey layout --> 
<androidx.appcompat.widget.Toolbar
        android:id="@+id/toolbar"
        android:minHeight="?attr/actionBarSize"
        android:layout_width="match_parent"
        android:layout_height="wrap_content"
        android:theme="@style/ThemeOverlay.AppCompat.Dark.ActionBar"
        android:background="@color/toolbar_color">
    </androidx.appcompat.widget.Toolbar>

```


__Menu__: 

```
<?xml version="1.0" encoding="utf-8"?>
<menu xmlns:android="http://schemas.android.com/apk/res/android"
    xmlns:app="http://schemas.android.com/apk/res-auto">

    <item android:title="Buscar"
        android:id="@+id/search_menu"
        app:showAsAction="always"
        android:icon="@drawable/ic_search">
    </item>

    <item android:title="Carrinho"
        android:id="@+id/cart_menu"
        app:showAsAction="always"
        android:icon="@drawable/ic_cart">
    </item>

    <item android:title="Configurações"
        android:id="@+id/config_menu">
    </item>

    <item android:title="Sobre"
        android:id="@+id/about_menu">
    </item>
</menu>
```

__Tema (para PopUpMenu)__: 

```
   <style name="PopMenuTheme" parent="ThemeOverlay.AppCompat.ActionBar">
        <item name="android:background">@color/orange_one</item>
        <item name="android:textColor">@color/black</item>
    </style>

```
__Tema principal (adicione no seu tema principal)__: 

```
 <!-- Tema para menus PopUp -->
        <item name="popupTheme">@style/PopMenuTheme</item>
```

__colors__: 

```
    <color name="toolbar_color">#7DA9CD</color>
    <color name="orange_one">#FFC107</color>

```

__Activity__:

```
    
class ToolBarActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_tool_bar)
        setSupportActionBar(findViewById(R.id.toolbar))
    }


    //Infla o menu
    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.menu_toolbar,menu)
        return true
    }


    //acoes dos botoes do menu
    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when(item.itemId){
            R.id.search_menu -> {Toast.makeText(this,"Clicado em Search",Toast.LENGTH_SHORT).show()}
            R.id.cart_menu -> {Toast.makeText(this,"Clicado em Cart",Toast.LENGTH_SHORT).show()}
            R.id.about_menu -> {Toast.makeText(this,"Clicado em About",Toast.LENGTH_SHORT).show()}
            R.id.config_menu -> {Toast.makeText(this,"Clicado em Config",Toast.LENGTH_SHORT).show()}
        }

        return super.onOptionsItemSelected(item)
    }


}

```


---

## Tela 4 - Controles de Seleção (Básico)

![alt text](screenshots/13.png)


__Layout__:

```
<?xml version="1.0" encoding="utf-8"?>
<LinearLayout xmlns:android="http://schemas.android.com/apk/res/android"
    xmlns:app="http://schemas.android.com/apk/res-auto"
    xmlns:tools="http://schemas.android.com/tools"
    android:layout_width="match_parent"
    android:layout_height="match_parent"
    android:orientation="vertical"
    tools:context=".SelectionControlsActivity">


    <androidx.appcompat.widget.SwitchCompat
        android:id="@+id/mySwitch"
        android:layout_width="wrap_content"
        android:layout_height="wrap_content"
        android:text="@string/ligar_desliga"
        android:layout_margin="16dp"
        android:checked="true"

        />


    <androidx.appcompat.widget.AppCompatCheckBox
        android:id="@+id/myCheckbox"
        android:layout_width="wrap_content"
        android:layout_height="wrap_content"
        android:layout_margin="16dp"
        android:text="@string/checkbox_label"

        />


    <RadioGroup
        android:id="@+id/radioGroup"
        android:layout_width="wrap_content"
        android:layout_height="wrap_content"
        android:layout_margin="16dp" >


        <androidx.appcompat.widget.AppCompatRadioButton
            android:id="@+id/option1"
            android:layout_width="wrap_content"
            android:layout_height="wrap_content"
            android:layout_margin="4dp"
            android:text="@string/opcao_1"
            />

        <androidx.appcompat.widget.AppCompatRadioButton
            android:id="@+id/option2"
            android:layout_width="wrap_content"
            android:layout_height="wrap_content"
            android:layout_margin="4dp"
            android:text="Opção 2"
            />


        <androidx.appcompat.widget.AppCompatRadioButton
            android:id="@+id/option3"
            android:layout_width="wrap_content"
            android:layout_height="wrap_content"
            android:layout_margin="4dp"
            android:text="Opção 3"
            />

    </RadioGroup>
</LinearLayout>


```

__Activity__: 

```


class SelectionControlsActivity : AppCompatActivity() {

    private lateinit var binding: ActivitySelectionControlsBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivitySelectionControlsBinding.inflate(layoutInflater)
        setContentView(binding.root)


        val switch = binding.mySwitch

        switch.setOnCheckedChangeListener { compoundButton, b ->
            if (b){
                // when switch button is checked
                Toast.makeText(this,"Switch is on!",Toast.LENGTH_SHORT).show()
            }else{
                // if switch button is unchecked
                Toast.makeText(this,"Switch off!",Toast.LENGTH_SHORT).show()
            }
        }


        val checkbox = binding.myCheckbox

        checkbox.setOnCheckedChangeListener { compoundButton, b ->
            if (b){
                // when switch button is checked
                Toast.makeText(this,"Checked!",Toast.LENGTH_SHORT).show()
            }else{
                // if switch button is unchecked
                Toast.makeText(this,"Not Checked!",Toast.LENGTH_SHORT).show()
            }
        }

        val radioGroup = binding.radioGroup
        radioGroup.setOnCheckedChangeListener { radioGroup, i ->
           when(i) {
               R.id.option1 -> {Toast.makeText(this,"Opção 1",Toast.LENGTH_SHORT).show()}
               R.id.option2 -> {Toast.makeText(this,"Opção 2",Toast.LENGTH_SHORT).show()}
               R.id.option3 -> {Toast.makeText(this,"Opção 3",Toast.LENGTH_SHORT).show()}
           }

        }


    }
}

```

---

## Tela 5 - Drawer Menu (Menu Lateral)

![alt text](screenshots/14.png)


Para o desenvolvimento da tela mostrada acima, são necessários alguns resources, sendo eles: 
    1. Layout para toolbar
    2. Layout para o Header da Drawer Menu
    3. Drawer Menu 
    4. DrawerLayout com uma NavigationView

__#1 - Layout para Toolbar__: 

```
    <?xml version="1.0" encoding="utf-8"?>
<LinearLayout
    xmlns:android="http://schemas.android.com/apk/res/android"
    android:orientation="vertical"
    android:layout_width="match_parent"
    android:layout_height="wrap_content">


    <androidx.appcompat.widget.Toolbar
        android:id="@+id/toolbar"
        android:layout_width="match_parent"
        android:layout_height="?attr/actionBarSize"
        android:background="@color/toolbar_color"
        android:theme="@style/ThemeOverlay.AppCompat.Dark.ActionBar">
    </androidx.appcompat.widget.Toolbar>

</LinearLayout>

```

__#2 - Layout para o Header da Drawer Menu__: 

```
    <?xml version="1.0" encoding="utf-8"?>
<LinearLayout xmlns:android="http://schemas.android.com/apk/res/android"
    android:orientation="vertical"
    android:layout_width="match_parent"
    android:layout_height="160dp"
    android:padding="16dp"
    android:gravity="bottom"
    android:theme="@style/ThemeOverlay.AppCompat.Dark"
    android:background="@drawable/nav_background">

    <ImageView
        android:layout_width="56dp"
        android:layout_height="56dp"
        android:layout_marginTop="8dp"
        android:src="@drawable/ic_baseline_person_24"
        />


    <TextView
        android:layout_width="wrap_content"
        android:layout_height="wrap_content"
        android:text="Edson Jr"
        android:textAppearance="@style/TextAppearance.AppCompat.Body1"
        android:layout_marginTop="8dp"
        />

    <TextView
        android:layout_width="wrap_content"
        android:layout_height="wrap_content"
        android:text="email@email.com"
        android:textAppearance="@style/TextAppearance.AppCompat.Body1"
        android:layout_marginTop="8dp"
        />
</LinearLayout>

```


__#3 - Drawer Menu__: 

```
<?xml version="1.0" encoding="utf-8"?>
<menu xmlns:android="http://schemas.android.com/apk/res/android">

    <group android:checkableBehavior="single">
        <item
            android:id="@+id/menu_inbox"
            android:title="Inbox"
            android:icon="@drawable/ic_email"
            />

        <item
            android:id="@+id/menu_starret"
            android:title="Favoritos"
            android:icon="@drawable/ic_favorites"

            />

        <item
            android:id="@+id/menu_sent_emails"
            android:title="E-mails enviados"
            android:icon="@drawable/ic_email_sent"
            />

    </group>

    <item
        android:id="@+id/submenu_outros"
        android:title="Outros">

        <menu>
            <item android:title="Lixeira"
                android:id="@+id/menu_trash"
                android:icon="@drawable/ic_trash" />

            <item android:title="Spam"
                android:id="@+id/menu_spam"
                android:icon="@drawable/ic_spam" />
        </menu>
    </item>

</menu>

```


__#4 - DrawerLayout com uma NavigationView__: 

```
<?xml version="1.0" encoding="utf-8"?>
<androidx.drawerlayout.widget.DrawerLayout
    xmlns:android="http://schemas.android.com/apk/res/android"
    xmlns:app="http://schemas.android.com/apk/res-auto"
    xmlns:tools="http://schemas.android.com/tools"
    android:layout_width="match_parent"
    android:layout_height="match_parent"
    android:fitsSystemWindows="true"
    tools:openDrawer="start"
    android:id="@+id/drawerLayout">

    <include layout="@layout/toolbar_layout" />

    <com.google.android.material.navigation.NavigationView
        android:id="@+id/navigationView"
        app:layout_constraintStart_toStartOf="parent"
        android:layout_width="wrap_content"
        android:layout_height="match_parent"
        android:layout_gravity="start"
        app:headerLayout="@layout/nav_header"
        app:menu="@menu/drawer_menu"
        />

</androidx.drawerlayout.widget.DrawerLayout>

```


__Activity__:

```
    
class DrawerActivity : AppCompatActivity() {

    private lateinit var binding: ActivityDrawerBinding
    private lateinit var toolbar: androidx.appcompat.widget.Toolbar

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityDrawerBinding.inflate(layoutInflater)
        setContentView(binding.root)


        //configurando a Toolbar
        this.toolbar = findViewById(R.id.toolbar)
        setSupportActionBar(toolbar)

        //menu na toolbar para chamar o drawer menu
        val toogle = ActionBarDrawerToggle(
            this,
            binding.drawerLayout,
            toolbar,
            R.string.open_drawer,
            R.string.close_drawer
        )
        binding.drawerLayout.addDrawerListener(toogle)
        toogle.syncState()


        //click do menu do drawer menu
        binding.navigationView.setNavigationItemSelectedListener {
            val id = it.itemId

            when(id){
                R.id.menu_inbox -> { Toast.makeText(this,"Clicado em Inbox",Toast.LENGTH_SHORT).show()}
                R.id.menu_starret -> { Toast.makeText(this,"Clicado em Favoritos",Toast.LENGTH_SHORT).show()}
                R.id.menu_sent_emails -> { Toast.makeText(this,"Clicado em emails enviados",Toast.LENGTH_SHORT).show()}
                R.id.menu_trash -> { Toast.makeText(this,"Clicado em Lixeira",Toast.LENGTH_SHORT).show()}
                R.id.menu_spam -> { Toast.makeText(this,"Clicado em Spam",Toast.LENGTH_SHORT).show()}
            }
            true
        }


    }

    //aplicando uma acao ao backpressed para fechar o drawer menu ao clicar em voltar
    override fun onBackPressed() {
        if(binding.drawerLayout.isDrawerOpen(GravityCompat.START)){
            binding.drawerLayout.closeDrawer(GravityCompat.START)

        }else {
            super.onBackPressed()
        }
    }
}

``` 


---

## Tela 6 - Alerts (Básico)

![alt text](screenshots/15.png)
![alt text](screenshots/16.png)

__Layout__:

Aqui, o layout corresponde somente aos botões que criam e chamam os _alert dialogs_ na tela.

```
<?xml version="1.0" encoding="utf-8"?>
<LinearLayout
    xmlns:android="http://schemas.android.com/apk/res/android"
    xmlns:app="http://schemas.android.com/apk/res-auto"
    xmlns:tools="http://schemas.android.com/tools"
    android:layout_width="match_parent"
    android:layout_height="match_parent"
    android:orientation="vertical"
    tools:context=".DialogsActivity">

    <Button
        android:id="@+id/btnAlert"
        android:layout_width="wrap_content"
        android:layout_height="wrap_content"
        android:layout_gravity="center"
        android:layout_margin="16dp"
        android:text="@string/alert_dialog"
        />



    <Button
        android:id="@+id/btnAlertItens"
        android:layout_width="wrap_content"
        android:layout_height="wrap_content"
        android:layout_gravity="center"
        android:layout_margin="16dp"
        android:text="@string/alert_items"
        />

</LinearLayout>

```

__Activity__: 

```

class DialogsActivity : AppCompatActivity() {

    private lateinit var binding: ActivityDialogsBinding
    private lateinit var alertDialog: AlertDialog.Builder
    private lateinit var dialogItens: AlertDialog.Builder

    private val items = arrayOf("Item 1","Item 2", "Item 3", "Item 4")

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityDialogsBinding.inflate(layoutInflater)
        setContentView(binding.root)

        createSimpleAlert()
        createAlertWithOptions()


        binding.btnAlert.setOnClickListener {
            alertDialog.show()

        }

        binding.btnAlertItens.setOnClickListener {
            dialogItens.show()
        }

    }

    private fun createSimpleAlert() {
        alertDialog = AlertDialog.Builder(this,R.style.AlertDialog)
        alertDialog.setMessage("Deseja Excluir?")

        alertDialog.setPositiveButton("Sim") { _,_ ->
            Toast.makeText(this,"Excluido",Toast.LENGTH_SHORT).show()
        }

        alertDialog.setNegativeButton("Não"){ _,_ ->
            Toast.makeText(this,"Operação abortada",Toast.LENGTH_SHORT).show()
        }

        alertDialog.create()
    }


    private fun createAlertWithOptions() {
        dialogItens = AlertDialog.Builder(this,R.style.AlertDialog)
        dialogItens.setTitle("Selecione a opção desejada")
        dialogItens.setSingleChoiceItems(items,-1) { _, itemIndex ->
            Toast.makeText(this,"Selecionado ${items[itemIndex]}",Toast.LENGTH_SHORT).show()
        }
        dialogItens.create()
        //setMultChoiceItems -> Cria o dialogo com opcoes no formato de checkbox

    }
}

```

---

## Tela 7 - ProgressBar

__Layout__:

```
<?xml version="1.0" encoding="utf-8"?>
<androidx.constraintlayout.widget.ConstraintLayout
    xmlns:android="http://schemas.android.com/apk/res/android"
    xmlns:app="http://schemas.android.com/apk/res-auto"
    xmlns:tools="http://schemas.android.com/tools"
    android:layout_width="match_parent"
    android:layout_height="match_parent"
    tools:context=".ProgressBarActivity">

    <!-- progress bar circular normal -->
    <ProgressBar
        android:id="@+id/circular_progress_bar_component"
        android:layout_width="wrap_content"
        android:layout_height="wrap_content"
        android:layout_margin="24dp"
        app:layout_constraintStart_toStartOf="parent"
        app:layout_constraintEnd_toEndOf="parent"
        app:layout_constraintTop_toTopOf="parent"
        android:visibility="gone"
        />


    <!-- progress bar na horizontal. Veja o estilo aplicado a ela -->
    <ProgressBar
        android:id="@+id/linear_progress_bar_component"
        android:layout_width="0dp"
        android:layout_height="wrap_content"
        android:layout_margin="24dp"
        app:layout_constraintStart_toStartOf="parent"
        app:layout_constraintEnd_toEndOf="parent"
        app:layout_constraintTop_toBottomOf="@id/circular_progress_bar_component"
        style="@style/Widget.AppCompat.ProgressBar.Horizontal"
        android:visibility="gone"
        />


    <Button
        android:id="@+id/start_circular_progress_bar"
        android:layout_width="wrap_content"
        android:layout_height="wrap_content"
        app:layout_constraintEnd_toEndOf="parent"
        app:layout_constraintStart_toStartOf="parent"
        app:layout_constraintBottom_toTopOf="@id/start_linear_progress_bar"
        android:layout_margin="16dp"
        android:text="Circurlar"
        />



    <Button
        android:id="@+id/start_linear_progress_bar"
        android:layout_width="wrap_content"
        android:layout_height="wrap_content"
        app:layout_constraintEnd_toEndOf="parent"
        app:layout_constraintStart_toStartOf="parent"
        app:layout_constraintBottom_toBottomOf="parent"
        android:layout_margin="16dp"
        android:text="Linear"
        />


</androidx.constraintlayout.widget.ConstraintLayout>

```

__Activity__: 

Aqui, para simular o comportamento da progress bar como se ela estivesse sendo executada durante algum processo de loading, foi utilizado _AsyncTask_, no entanto, é importante informar que _AsyncTask_ está depreciado. Uso somente para fins ilustrativos.

```

class ProgressBarActivity : AppCompatActivity() {

    private lateinit var binding: ActivityProgressBarBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityProgressBarBinding.inflate(layoutInflater)
        setContentView(binding.root)



        binding.startCircularProgressBar.setOnClickListener {
            asyncTask(binding.circularProgressBarComponent).execute()
        }

        binding.startLinearProgressBar.setOnClickListener {
            binding.linearProgressBarComponent.setProgress(0)
            binding.linearProgressBarComponent.max = 100
            asyncTask(binding.linearProgressBarComponent).execute()

        }

    }


    //Criando uma task em background
    class asyncTask(private val progressBar: ProgressBar): AsyncTask<Void,Int,Void>() {

        override fun doInBackground(vararg p0: Void?): Void? {
            for(i in 0..100){
                try {
                    publishProgress(i)
                    Thread.sleep(100)
                }catch (e: InterruptedException) {
                    Log.e("ProgressBar",e.printStackTrace().toString())
                }

            }
            return null
        }

        override fun onPreExecute() {
            progressBar.visibility = View.VISIBLE
        }

        override fun onPostExecute(result: Void?) {
            progressBar.visibility = View.GONE
        }

        override fun onProgressUpdate(vararg values: Int?) {
            progressBar.progress = values[0]!!
        }

    }
}

```

---


## Tela 8 - Bottom Navigation (Básico)

Bottom Navigation básica, sem o uso de _Navigation Component_.

![alt text](screenshots/17.png)

Para a construção da bottom navigation, é necessário a construção de um arquivo de menu. 

__Menu Bottom Navigation__:

```
<?xml version="1.0" encoding="utf-8"?>
<menu xmlns:android="http://schemas.android.com/apk/res/android">

    <item android:id="@+id/menu_home" android:title="Home" android:icon="@drawable/ic_home" />
    <item android:id="@+id/menu_fav" android:title="Favoritos" android:icon="@drawable/ic_fav" />
    <item android:id="@+id/menu_location" android:title="Localização" android:icon="@drawable/ic_location" />

</menu>

```


__Layout__:

```
<?xml version="1.0" encoding="utf-8"?>
<androidx.constraintlayout.widget.ConstraintLayout
    xmlns:android="http://schemas.android.com/apk/res/android"
    xmlns:app="http://schemas.android.com/apk/res-auto"
    xmlns:tools="http://schemas.android.com/tools"
    android:layout_width="match_parent"
    android:layout_height="match_parent"
    tools:context=".BottomNavigationActivity">


    <com.google.android.material.bottomnavigation.BottomNavigationView
        android:id="@+id/bottom_navigation"
        android:layout_width="match_parent"
        android:layout_height="wrap_content"
        app:layout_constraintBottom_toBottomOf="parent"
        android:background="@color/red"
        app:menu="@menu/bottom_navigation_bar_menu"
        app:itemIconTint="@color/white"
        app:itemTextColor="@color/white"
        />

</androidx.constraintlayout.widget.ConstraintLayout>

```


__Activity__: 

```
class BottomNavigationActivity : AppCompatActivity() {

    private lateinit var binding: ActivityBottomNavigationBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityBottomNavigationBinding.inflate(layoutInflater)
        setContentView(binding.root)
        
        //Responsavel pelos clicks nos itens da bottom navigation
        binding.bottomNavigation.setOnItemSelectedListener { menuId ->
            when(menuId.itemId){
                R.id.menu_home -> {Toast.makeText(this,"Menu_Home",Toast.LENGTH_SHORT).show()}
                R.id.menu_fav -> {Toast.makeText(this,"Menu_favoritos",Toast.LENGTH_SHORT).show()}
                R.id.menu_location -> {Toast.makeText(this,"Menu_Localização",Toast.LENGTH_SHORT).show()}
            }
            return@setOnItemSelectedListener true
        }

    }
}

```

---

## Tela 8 - Collapsing Layout (Básico)

![alt text](screenshots/18.gif)  
  
O Collapsing layout permite animar partes do layout ao ponto em que o elemento em questão possa ser "escondido" e "mostrado" de acordo com eventos de scrool na tela, como os que estão na figura acima.   
  
Para a criação deste tipo de tela, é utilizado um _CoordinatorLayout_, bem como um _CollapsingToolbarLayout_, caso esteja também fazendo uso de uma Toolbar. No caso da criação de uma Toolbar no seu layout, é recomendado a criação de um novo tema, para dar o efeito como descrito na imagem acima.  Este tema também será configurado na AndroidManifest. Para a construção da toolbar, também é necessário a criação de um arquivo de menu.
  
Abaixo do _CoordinatorLayout_ você pode criar outros tipos de layout que irão compor o restante da tela.

__Tema__: 


```
<!-- Tema para o collapsing layout -->
    <style name="CollapsingLayoutStyle" parent="Theme.AppCompat.Light.NoActionBar">
        <item name="windowNoTitle">true</item>
        <item name="android:windowDrawsSystemBarBackgrounds">true</item>
        <!-- Deixa a status bar transparent -->
        <item name="android:windowTranslucentStatus">true</item>
        <item name="android:windowTranslucentNavigation">true</item>
    </style>

```


__Configuração do tema no AndroidManifest__: 

```
       <activity
            android:name=".CollapsingLayoutActivity"
            android:exported="false"
            android:theme="@style/CollapsingLayoutStyle"
            />
       <activity
```


__Menu Toolbar__: 

```

 <?xml version="1.0" encoding="utf-8"?>
<menu xmlns:android="http://schemas.android.com/apk/res/android"
    xmlns:app="http://schemas.android.com/apk/res-auto">

    <item android:id="@+id/menu_editar_collapsing"
        android:title="Editar"
        app:showAsAction="always"
        android:icon="@drawable/ic_edit"
        />


    <item android:id="@+id/menu_favoritar_collpasing"
        android:title="Favoritar"
        app:showAsAction="always"
        android:icon="@drawable/ic_fav_2"
        />

</menu>

```


__Layout__:

```
<?xml version="1.0" encoding="utf-8"?>
<androidx.coordinatorlayout.widget.CoordinatorLayout
    xmlns:android="http://schemas.android.com/apk/res/android"
    xmlns:app="http://schemas.android.com/apk/res-auto"
    xmlns:tools="http://schemas.android.com/tools"
    android:layout_width="match_parent"
    android:layout_height="match_parent"
    android:fitsSystemWindows="true"
    tools:context=".CollapsingLayoutActivity">


    <com.google.android.material.appbar.AppBarLayout
        android:id="@+id/appbarLayout"
        android:fitsSystemWindows="true"
        android:layout_width="match_parent"
        android:layout_height="wrap_content"
        style="@style/ThemeOverlay.AppCompat.Dark.ActionBar">

        <com.google.android.material.appbar.CollapsingToolbarLayout
            android:id="@+id/collapse_layout"
            android:layout_width="match_parent"
            android:layout_height="match_parent"
            android:fitsSystemWindows="true"
            app:contentScrim="@color/toolbar_color"
            app:layout_scrollFlags="scroll|exitUntilCollapsed"
            app:expandedTitleGravity="center|bottom"
            app:expandedTitleTextAppearance="@style/TextAppearance.AppCompat.Headline" >


            <ImageView
                android:layout_width="match_parent"
                android:layout_height="300dp"
                android:src="@drawable/nav_background"
                app:layout_collapseMode="parallax"
                android:scaleType="centerCrop"
                android:fitsSystemWindows="true"
                />

            <androidx.appcompat.widget.Toolbar
                android:id="@+id/toolbar"
                android:layout_width="match_parent"
                android:layout_height="?attr/actionBarSize"
                app:layout_collapseMode="pin"
                />
        </com.google.android.material.appbar.CollapsingToolbarLayout>
    </com.google.android.material.appbar.AppBarLayout>

  <androidx.core.widget.NestedScrollView
      app:layout_behavior="com.google.android.material.appbar.AppBarLayout$ScrollingViewBehavior"
      android:layout_width="match_parent"
      android:layout_height="match_parent">

      <androidx.constraintlayout.widget.ConstraintLayout
          android:id="@+id/contentLayout"
          android:layout_width="match_parent"
          android:layout_height="match_parent">

          <TextView
              android:id="@+id/txt_personal_infos"
              android:layout_width="wrap_content"
              android:layout_height="wrap_content"
              app:layout_constraintStart_toStartOf="parent"
              app:layout_constraintTop_toTopOf="parent"
              android:text="Informações pessoais"
              android:textSize="20sp"
              android:textStyle="bold"
              android:textColor="@color/personal_data_text_color"
              android:fontFamily="sans-serif-condensed-medium"
              android:layout_marginTop="16dp"
              android:layout_marginStart="12dp"
              android:layout_marginEnd="4dp"
              />



          <ImageView
              android:id="@+id/phone_ic"
              android:layout_width="32dp"
              android:layout_height="32dp"
              android:src="@drawable/ic_phone"
              android:layout_marginTop="16dp"
              android:layout_marginStart="12dp"
              android:layout_marginEnd="4dp"
              app:layout_constraintStart_toStartOf="parent"
              app:layout_constraintTop_toBottomOf="@id/txt_personal_infos"
              app:tint="@color/personal_data_text_color" />


          <TextView
              android:id="@+id/txt_phone_number"
              android:layout_width="wrap_content"
              android:layout_height="wrap_content"
              android:text="(092) 3621-0704"
              android:textSize="20sp"
              android:textColor="@color/personal_data_text_color"
              android:fontFamily="sans-serif-condensed-medium"
              android:layout_marginStart="16dp"
              app:layout_constraintStart_toEndOf="@id/phone_ic"
              app:layout_constraintTop_toTopOf="@id/phone_ic"
              />



          <ImageView
              android:id="@+id/email_ic"
              android:layout_width="32dp"
              android:layout_height="32dp"
              android:src="@drawable/ic_email_black"
              android:layout_marginTop="16dp"
              android:layout_marginStart="12dp"
              android:layout_marginEnd="4dp"
              app:layout_constraintStart_toStartOf="parent"
              app:layout_constraintTop_toBottomOf="@id/phone_ic"
              app:tint="@color/personal_data_text_color" />


          <TextView
              android:id="@+id/txt_email_address"
              android:layout_width="wrap_content"
              android:layout_height="wrap_content"
              android:text="fulano@fulano.com"
              android:textSize="20sp"
              android:textColor="@color/personal_data_text_color"
              android:fontFamily="sans-serif-condensed-medium"
              android:layout_marginStart="16dp"
              app:layout_constraintStart_toEndOf="@id/email_ic"
              app:layout_constraintTop_toTopOf="@id/email_ic"
              />


          <ImageView
              android:id="@+id/location_ic"
              android:layout_width="32dp"
              android:layout_height="32dp"
              android:src="@drawable/ic_location"
              android:layout_marginTop="16dp"
              android:layout_marginStart="12dp"
              android:layout_marginEnd="4dp"
              app:layout_constraintStart_toStartOf="parent"
              app:layout_constraintTop_toBottomOf="@id/email_ic"
              app:tint="@color/personal_data_text_color" />


          <TextView
              android:id="@+id/txt_location"
              android:layout_width="wrap_content"
              android:layout_height="wrap_content"
              android:text="Rua não sei das quantas"
              android:textSize="20sp"
              android:textColor="@color/personal_data_text_color"
              android:fontFamily="sans-serif-condensed-medium"
              android:layout_marginStart="16dp"
              app:layout_constraintStart_toEndOf="@id/location_ic"
              app:layout_constraintTop_toTopOf="@id/location_ic"
              />
      </androidx.constraintlayout.widget.ConstraintLayout>
  </androidx.core.widget.NestedScrollView>

    <!-- Float action button
    <com.google.android.material.floatingactionbutton.FloatingActionButton
        android:backgroundTint="@color/pink_floatButton"
        android:layout_width="wrap_content"
        android:layout_height="wrap_content"
        android:src="@drawable/ic_phone_call"
        app:layout_anchor="@id/appbarLayout"
        app:layout_anchorGravity="end|bottom"
        android:layout_marginEnd="8dp"
        />

        -->




</androidx.coordinatorlayout.widget.CoordinatorLayout>

```


__Activity__:


```

class CollapsingLayoutActivity : AppCompatActivity() {

    private lateinit var binding: ActivityCollapsingLayoutBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityCollapsingLayoutBinding.inflate(layoutInflater)
        setContentView(binding.root)

        setSupportActionBar(binding.toolbar)
        supportActionBar?.setDisplayHomeAsUpEnabled(true) //adiciona o botao de voltar na toolbar
        binding.toolbar.setNavigationOnClickListener { onBackPressed() } // executa o backpressed ao clicar na seta de voltar na toolbar
    }


    //Infla e carrega o menu
    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.collapsing_menu_toolbar,menu)
        return super.onCreateOptionsMenu(menu)
    }


    //acoes de cliques do menu da toolbar
    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when(item.itemId){
            R.id.menu_editar_collapsing -> {Toast.makeText(this,"Selecionado editar",Toast.LENGTH_SHORT).show()}
            R.id.menu_favoritar_collpasing -> {Toast.makeText(this,"Selecionado favoritar",Toast.LENGTH_SHORT).show()}
        }
        return true
    }


}


```
---


## Tela 9 - ViewPager 2 c/ Fragments e Tabmediator

![alt text](screenshots/19.png)

__ViewPager__ é uma lib Android que permite o usuário realizar rolagem (troca) através de páginas de dados. Estas páginas podem ser fragmentos. A ViewPager 2 é uma continuação da ViewPager, oferecendo aprimoramentos.
__TabMediator__ é utilizado em conjunto com a ViewPager2 e serve para sincronizar a posição das páginas de dados que estão sendo manipuladas pela ViewPager2. O Tabmediator também oferece guias para que o usuário possa selecionar qual página de dados deseja visualizar.   
  
Aqui neste exemplo foram utilizados o ViewPager 2 e o TabMediator. As páginas de dados são representadas por Fragments. Aqui são usados dois Fragments e uma activity para elcuidar este exmeplo. O ViewPager também necessita de um adapter para funcionar.


__Layout Activity__: 

```

<?xml version="1.0" encoding="utf-8"?>
<LinearLayout
    xmlns:android="http://schemas.android.com/apk/res/android"
    xmlns:app="http://schemas.android.com/apk/res-auto"
    xmlns:tools="http://schemas.android.com/tools"
    android:layout_width="match_parent"
    android:layout_height="match_parent"
    tools:context=".views.ViewPagerActivity"
    android:orientation="vertical">

    <com.google.android.material.tabs.TabLayout
        android:id="@+id/tabLayout"
        android:layout_width="match_parent"
        android:layout_height="wrap_content"

        />


    <androidx.viewpager2.widget.ViewPager2
        android:id="@+id/view_pager"
        android:layout_width="match_parent"
        android:layout_height="match_parent"
        android:orientation="horizontal"
        />

</LinearLayout>

```

__Activity__: 


```
class ViewPagerActivity : AppCompatActivity() {

    private lateinit var binding: ActivityViewPagerBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityViewPagerBinding.inflate(layoutInflater)
        setContentView(binding.root)

        //Criando o adapter para trabalhar com o ViewPager
        val viewPagerAdapter = SimpleViewPagerAdapter(
            activity = this,
            argumentsToFragment = articleDataStore)
        binding.viewPager.adapter = viewPagerAdapter

        //configurando o tablayout
        val tabTitles: MutableList<Pair<String,Int>> = ArrayList()
        tabTitles.add(Pair("Frag 1",R.drawable.ic_dot))
        tabTitles.add(Pair("Frag 2",R.drawable.ic_dot))
        TabLayoutMediator(binding.tabLayout,binding.viewPager){ tab,position ->
            tab.text = tabTitles[position].first
            tab.setIcon(tabTitles[position].second)
        }.attach()



    }
}

```

__Fragment 2 - Layout__: 

```
?xml version="1.0" encoding="utf-8"?>
<androidx.constraintlayout.widget.ConstraintLayout xmlns:android="http://schemas.android.com/apk/res/android"
    xmlns:tools="http://schemas.android.com/tools"
    android:layout_width="match_parent"
    android:layout_height="match_parent"
    xmlns:app="http://schemas.android.com/apk/res-auto"
    tools:context=".views.fragments.ViewPagerFragmentOne">


    <TextView
        android:id="@+id/txt_fragment_identification"
        android:layout_width="match_parent"
        android:layout_height="wrap_content"
        app:layout_constraintTop_toTopOf="parent"
        android:text="View Pager - Fragment 2"
        android:textStyle="bold"
        android:textColor="@color/black"
        android:textSize="21sp"
        android:layout_margin="16dp"
        />

    <androidx.recyclerview.widget.RecyclerView
        android:id="@+id/recyclerView"
        android:layout_width="match_parent"
        android:layout_height="0dp"
        tools:listitem="@layout/item_recyclerview"
        app:layout_constraintTop_toBottomOf="@id/txt_fragment_identification"
        app:layout_constraintBottom_toBottomOf="parent"
        android:layout_marginTop="16dp"
        />


</androidx.constraintlayout.widget.ConstraintLayout>

```

#### Obs: O fragment 1 possui somente um TextView no layout.

__RecyclerView Item Layout__:  
  

```
<?xml version="1.0" encoding="utf-8"?>
<androidx.cardview.widget.CardView xmlns:android="http://schemas.android.com/apk/res/android"
    android:layout_width="match_parent"
    android:layout_height="wrap_content"
    xmlns:app="http://schemas.android.com/apk/res-auto"
    app:cardCornerRadius="4dp"
    app:cardElevation="8dp"
    android:layout_margin="16dp">

    <LinearLayout
        android:layout_width="match_parent"
        android:layout_height="match_parent"
        android:orientation="vertical">

        <ImageView
            android:id="@+id/featureImage"
            android:layout_width="match_parent"
            android:layout_height="200dp"
            android:layout_margin="16dp"
            android:src="@color/cardview_dark_background" />

        <LinearLayout
            android:layout_width="match_parent"
            android:layout_height="wrap_content"
            android:orientation="vertical"
            android:layout_marginTop="24dp"
            android:layout_marginLeft="16dp"
            android:layout_marginBottom="16dp"
            android:layout_marginRight="16dp">

            <TextView
                android:id="@+id/title"
                android:layout_width="match_parent"
                android:layout_height="wrap_content"
                android:text="Título do Post"
                android:textAppearance="@android:style/TextAppearance.Large"
                />

            <TextView
                android:id="@+id/desc"
                android:layout_width="match_parent"
                android:layout_height="wrap_content"
                android:text="Descrição"
                android:layout_marginTop="8dp"
                android:textAppearance="@android:style/TextAppearance.Small" />

        </LinearLayout>

       <androidx.appcompat.widget.AppCompatButton
           android:id="@+id/btnLink"
           android:layout_width="wrap_content"
           android:layout_height="wrap_content"
           style="@style/Widget.AppCompat.Button.Borderless"
           android:layout_margin="8dp"
           android:text="Acessar" />

    </LinearLayout>


</androidx.cardview.widget.CardView>

```


__RecyclerView Adapter__: 

```

class RecyclerAdapter: RecyclerView.Adapter<ViewHolder>() {

    private var articleList: MutableList<Article> = mutableListOf()
    var accessButtonListener: (String) -> Unit = {}

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val binding =  ItemRecyclerviewBinding.inflate(LayoutInflater.from(parent.context),parent,false)
        return ViewHolder(binding)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(articleList.get(position),accessButtonListener)
    }

    override fun getItemCount(): Int = articleList.size


    fun updateRecyclerView(articleList: MutableList<Article>){
        if(!articleList.isNullOrEmpty()){
            this.articleList.clear()
        }
        this.articleList = articleList
    }
}





class ViewHolder(private val itemBinding: ItemRecyclerviewBinding)
    : RecyclerView.ViewHolder(itemBinding.root) {

        fun bind(item: Article, clickCallback: (String) -> Unit = {}) {
            itemBinding.title.text = item.title
            itemBinding.desc.text = item.description
            itemBinding.featureImage.load(item.image){
                crossfade(true)
                placeholder(R.drawable.ic_no_image)
                //transformations(CircleCropTransformation()) - Deixa a imagem redonda
            }

            itemBinding.btnLink.setOnClickListener {
                //callback do click do botao de acessar
                clickCallback.invoke(item.title)
            }
        }
}

```


__PagerView Adapter__:  
  
```  
const val NUMBER_OF_FRAGMENTS = 2
class SimpleViewPagerAdapter(activity: AppCompatActivity,
                             private val argumentsToFragment: List<Article>): FragmentStateAdapter(activity){

    override fun getItemCount(): Int = NUMBER_OF_FRAGMENTS

    override fun createFragment(position: Int): Fragment {
        var fragment = Fragment()
        var bundle = Bundle()
        when(position) {
            0 -> {
                fragment = ViewPagerFragmentOne()
                bundle.putSerializable(VIEW_PAGER_FRAGMENT_KEY,argumentsToFragment as Serializable)
                fragment.arguments = bundle
               }
            1 -> {
                fragment =  ViewPagerFragmentTwo()
                bundle.putSerializable(VIEW_PAGER_FRAGMENT_KEY,argumentsToFragment as Serializable)
                fragment.arguments = bundle
                }
        }
        return fragment
    }
}


```


__Fragment 2__: 

```
lass ViewPagerFragmentTwo : Fragment() {

    private var _binding: FragmentViewPagerTwoBinding? = null
    private val binding get() = _binding!!
    private var args: List<Article>? = null
    private lateinit var recyclerViewAdapter: RecyclerAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = FragmentViewPagerTwoBinding.inflate(inflater, container, false)
        getAguments()
        return binding.root
    }

    private fun getAguments() {
        args = arguments?.get(VIEW_PAGER_FRAGMENT_KEY) as List<Article>?
        if(!args.isNullOrEmpty()){
            setupRecyclerViewAdapter()
        }
    }

    private fun setupRecyclerViewAdapter(){
        recyclerViewAdapter = RecyclerAdapter()
        recyclerViewAdapter.updateRecyclerView(args as MutableList<Article>)
        binding.recyclerView.adapter = recyclerViewAdapter
        binding.recyclerView.layoutManager = LinearLayoutManager(requireContext())
        recyclerViewAdapter.accessButtonListener = { name ->
            Toast.makeText(requireContext(),"Acessando: $name", Toast.LENGTH_SHORT).show()
        }

    }
}

```


---



